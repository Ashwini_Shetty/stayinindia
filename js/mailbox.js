var app = angular.module('sapthakotiResidencyApp', ['ngRoute']);

app.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl : '/home.html',
    controller  : 'HomeController'
  })
  
});

app.controller('HomeController', function($scope) {
  $scope.submitMessage = function(){
	  console.log("inside the main page");
  }
});
